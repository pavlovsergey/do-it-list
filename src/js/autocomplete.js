export  function autocomplete (input, source, handle) {
    let el = $(input);
    el.autocomplete({source: source});
    //Event select
    el.autocomplete({
        select: function(event, ui) { handle(ui);
        /*ui.item будет содержать выбранный элемент*/
        }
    });

}

export  function updAutocomplete (input, source) {
    let el = $(input);
    el.autocomplete( "option", "source", source );
}
