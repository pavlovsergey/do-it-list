import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteTask } from '../AC';
import Task from './Task';

class TaskList extends Component {
    constructor(props) {
        super(props);
    }
    render () {
        const taskElements = this.props.tasks.map(
            (task) => <li key= {task.id}>
                <Task
                    task = {task}
                    deleteTask = {this.props.deleteTask}
                />
            </li>
        );
        return (
            <ul > { taskElements } </ul>
        );
    }
}

TaskList.propTypes = {
    //from store
    tasks: PropTypes.array.isRequired,
    deleteTask: PropTypes.func.isRequired
};


export default connect((state) => {
    if (!state.filterTask)  { return { tasks: state.tasks}; }
    else {
        return { tasks: state.tasks.filter(task =>
        {return task.id == state.filterTask; })
        };
    }
}, { deleteTask })(TaskList);

