import {DELETE_TASK, ADD_TASK, CHANGE_FILTER} from '../constants';

export function deleteTask(id) {
    return {
        type: DELETE_TASK,
        payload: { id }
    };
}

export function addTask(name) {
    return {
        type: ADD_TASK,
        payload: { name }
    };
}

export function changeFilter(id) {
    return {
        type: CHANGE_FILTER,
        payload: { id }
    };
}

