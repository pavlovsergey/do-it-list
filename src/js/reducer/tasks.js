import {DELETE_TASK, ADD_TASK} from '../constants';

export default (tasksState = [] , action) => {
    const {type, payload, taskId} = action;

    switch (type) {

    case DELETE_TASK:
        return tasksState.filter((task)=> { return task.id !== payload.id; });

    case ADD_TASK:
        return [...tasksState, {'id':taskId, 'label':payload.name}];
    }
    return tasksState;
};
