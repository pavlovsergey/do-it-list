import {createStore, applyMiddleware} from 'redux';
import reducer from '../reducer';
import taskId from '../middlewares/taskId';

const store = createStore(reducer, applyMiddleware(taskId));

export default store;
