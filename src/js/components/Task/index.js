import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './task.css';

export default class Task extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isDone: false
        };
    }

    render () {
        const { label } = this.props.task;
        return (
            //id,
            <div className = {this.state.isDone? 'taskdone' : 'task'} >
                <label>
                    <input type = "checkbox"
                        checked = {this.state.isDone}
                        onChange = {this.toggleIsDone.bind(this)}
                    />
                    {label}
                </label>

                <button onClick = {this.handleDeleteTask.bind(this)} > Delete </button>
            </div>
        );
    }

    toggleIsDone () {
        this.setState({
            isDone: !this.state.isDone
        });
    }

    handleDeleteTask () {
        const {task ,deleteTask} = this.props;
        deleteTask(task.id);
    }
}

Task.propTypes = {
    task: PropTypes.shape({
        id: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
    }).isRequired,
    deleteTask: PropTypes.func.isRequired
};

