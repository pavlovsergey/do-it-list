import {CHANGE_FILTER} from '../constants';

export default (filter = '', action) => {
    const { type, payload } = action;

    switch (type) {
    case CHANGE_FILTER:  return payload.id;
    }

    return filter;
};
