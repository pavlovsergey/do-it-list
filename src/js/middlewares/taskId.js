import { ADD_TASK } from '../constants';
export default store => next => action => {
    if (!(action.type == ADD_TASK)) return next(action);
    return next(Object.assign({}, action, { taskId : Date.now().toString() })
    );
};
