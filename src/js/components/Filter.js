import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { autocomplete, updAutocomplete } from '../autocomplete';
import { connect } from 'react-redux';
import { changeFilter } from '../AC';

class Filter extends Component {

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    shouldComponentUpdate(nextProps) {

        updAutocomplete (this.inputEl, nextProps.tasks);
        return false;
    }

    render() {

        return (
            <div>
                <input type = "text"
                    ref = {(node) => {this.inputEl = node;}}
                    onChange = {this.handleChange}
                    placeholder = 'Find task'
                />
            </div>
        );
    }

    handleChange () {
        if (this.props.filter) {this.props.changeFilter('');}
    }

    handleSelect (id) {
        this.props.changeFilter(id.item.id);
    }

    componentDidMount() {
        const { tasks } = this.props.tasks;
        autocomplete (this.inputEl, tasks||[], this.handleSelect.bind(this));
    }
}

Filter.propTypes = {
    //from connect
    tasks: PropTypes.array.isRequired,
    filter : PropTypes.string.isRequired,
    changeFilter: PropTypes.func.isRequired
};

export default connect((state) => ({
    tasks: state.tasks, filter: state.filterTask}), { changeFilter })(Filter);






