import {combineReducers} from 'redux';
import tasks from './tasks';
import filterTask from './filter';

export default combineReducers({ tasks, filterTask });
