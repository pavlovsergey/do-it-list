import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addTask } from '../AC';

class FormAddTask  extends Component {
    constructor(props){
        super(props);
        this.state = {valueTask:''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange (event) {
        this.setState({valueTask: event.target.value});
    }

    handleSubmit (event) {
        event.preventDefault();
        if (this.state.valueTask) {
            this.props.addTask(this.state.valueTask);
            this.setState({
                valueTask: ''
            });
        }
    }

    render (){
        return (
            <form onSubmit = {this.handleSubmit}>
                <input type = "text"
                    value = {this.state.valueTask}
                    onChange = {this.handleChange}
                    placeholder = 'Write a problem'
                />
                <input type = "submit" value = "Add Task" />
            </form>
        );
    }
}

FormAddTask.propTypes = {
    //from store
    addTask: PropTypes.func.isRequired
};

export default connect( null , { addTask })(FormAddTask);
