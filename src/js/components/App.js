import React from 'react';
import TaskList from './TaskList';
import FormAddTask from './FormAddTask';
import Filter from './Filter';

function App() {
    return (
        <div className = 'wrapper'>
            <Filter />
            <h1> To Do It List </h1>
            <TaskList />
            <FormAddTask />
        </div>
    );
}

export default App;
